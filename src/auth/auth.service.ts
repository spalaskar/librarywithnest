import {
  Injectable,
  NotFoundException,
  HttpException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AuthDto } from './dto/auth.dto';
import { AuthDocument } from './schema/auth.schema';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel('auths') private authModel: Model<AuthDocument>,
    private jwtService: JwtService,
  ) {}

  async protectedRoute() {
    return 'Protected routes';
  }

  async registration(authDto: AuthDto): Promise<AuthDocument> {
    const { email, password } = authDto;
    if (!email || !password)
      throw new HttpException('Please enter email and password', 500);

    const hash = await bcrypt.hash(password, 10);
    const data = {
      email,
      password: hash,
    };
    const user = await this.authModel.create(data);

    if (!user) throw new HttpException('Something went wrong', 500);
    return user;
  }

  async login(authDto: AuthDto) {
    const { email, password } = authDto;

    if (!email || !password)
      throw new HttpException('Please enter email and password', 500);

    const query = { email: email };
    const user = await this.authModel.findOne(query);
    if (!user) throw new NotFoundException('Email Does not exist');

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) throw new UnauthorizedException('Password is not match');

    return this.signUser(authDto);
  }

  signUser(authDto: AuthDto) {
    const payload = { email: authDto.email };
    return { access_token: this.jwtService.sign(payload) };
  }
}
