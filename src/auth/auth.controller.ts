import { Body, Controller, Post, Res, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { AuthDto } from './dto/auth.dto';

@ApiTags('Auth')
//@ApiBearerAuth()
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('/registration')
  
  async registration(@Res() res, @Body() authDto: AuthDto) {
    const book = await this.authService.registration(authDto);
    return res.json({ response: book });
  }

  @Post('/login')
  async login(@Res() res, @Body() authDto: AuthDto) {
    const book = await this.authService.login(authDto);
    return res.json({ response: book });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/protectedRoute')
  @ApiBearerAuth()
  async protectedRoute() {
    return this.authService.protectedRoute();
    //  return res.json({ response: book });
  }
}
