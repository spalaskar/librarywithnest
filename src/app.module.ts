import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BooksModule } from './books/books.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    BooksModule,
    AuthModule,
    MongooseModule.forRoot('mongodb://mongo:27017/Library'),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}


