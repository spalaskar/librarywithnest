import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BookDto } from './dto/book.dto';
import { bookInterface } from './interface/book.interface';

@Injectable()
export class BooksService {
  constructor(@InjectModel('book') private bookModel: Model<bookInterface>) {}

  async getBook() {
    return this.bookModel.find();
  }
  async createBook(bookDto: BookDto) {
    const book = new this.bookModel(bookDto);
    return await book.save();
  }
  async getBookById(id: string) {
    return this.bookModel.findById(id);
  }
  async deleteBookById(id: string) {
    return this.bookModel.findByIdAndDelete(id);
  }
}
