import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class BookDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()

  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()

  readonly price: string;
}
