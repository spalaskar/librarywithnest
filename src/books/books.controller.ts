import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Res,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
import { BooksService } from './books.service';
import { BookDto } from './dto/book.dto';

@ApiTags('Books')
@Controller('books')
export class BooksController {
  constructor(private bookService: BooksService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('/getAllBooks')
  @ApiBearerAuth()
  async getBook(@Res() res) {
    const book = await this.bookService.getBook();
    return res.json({ response: book });
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('/addBook')
  @UsePipes(new ValidationPipe())
  @ApiBearerAuth()
  async createBook(@Res() res, @Body() bookDto: BookDto) {
    const book = await this.bookService.createBook(bookDto);
    return res.json({ response: book });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/getBookById/:id')
  @ApiParam({ name: 'id' })
  @ApiBearerAuth()
  getBookById(@Param('id') id: string) {
    return this.bookService.getBookById(id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/deleteBookById/:id')
  @ApiParam({ name: 'id' })
  @ApiBearerAuth()
  deleteBookById(@Param('id') id: string) {
    return this.bookService.deleteBookById(id);
  }
}
